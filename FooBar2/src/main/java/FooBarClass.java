public class FooBarClass {

    static void FooBarClass(int arg) {
        String s1, s2;

        if (arg > 0 ){

            FooBarClass(arg-1);

            s1 = "";
            s2 = "";

            if (arg % 3 == 0) s1 = "Foo";

            if (arg % 5 == 0) s2 = "Bar";

            System.out.println(arg + " " +s1+s2);

        }
    }
}
