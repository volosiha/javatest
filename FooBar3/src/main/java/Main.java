///////////////////////////////////////////////////////////////////////////////////////
// минизация  циклов за счет обработки нескольких чисел одновременно - блоками по 5  //
///////////////////////////////////////////////////////////////////////////////////////
public class Main {

    public static void main(String[] args) {
        int n = Integer.parseInt(args[0]);

        if (n > 0) {
            String s1, s2;
            int c;

            c = 0;

            for (int i = 5; i <= n; i = i + 5) {

                switch (c) {
                    case 0:
                        System.out.println(i - 4);
                        System.out.println(i - 3);
                        System.out.println(i - 2 + " Foo");
                        System.out.println(i - 1);
                        System.out.println(i + " Bar");
                        break;

                    case 1:
                        System.out.println(i - 4 + " Foo");
                        System.out.println(i - 3);
                        System.out.println(i - 2);
                        System.out.println(i - 1 + " Foo");
                        System.out.println(i + " Bar");
                        break;

                    case 2:
                        System.out.println(i - 4);
                        System.out.println(i - 3 + " Foo");
                        System.out.println(i - 2);
                        System.out.println(i - 1);
                        System.out.println(i + " FooBar");
                        break;
                }

                c = (c + 1) % 3;
            }

            for (int j = n - n % 5 + 1; j <= n; j++) {
                if (j % 3 == 0) System.out.println(j + " Foo");
                else System.out.println(j);
            }
        }
        else {
            System.out.println("Входной параметр должен быть целым числом больше 0.");
        }

    }
}
