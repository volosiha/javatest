<%@ page import="java.sql.Statement" %>
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="java.sql.DriverManager" %>
<%@ page import="DBPackage.DBAdapter2" %>

<%--
  Created by IntelliJ IDEA.
  User: shuyupovayo
  Date: 05.02.2021
  Time: 16:17
  To change this template use File | Settings | File Templates.
--%>
<%--#DataSourceSettings#
#LocalDataSource: mydbs.sqlite
#BEGIN#
<data-source source="LOCAL" name="mydbs.sqlite" uuid="98e20567-0058-4458-9a83-5d5c68a61f8d"><database-info product="SQLite" version="3.34.0" jdbc-version="2.1" driver-name="SQLite JDBC" driver-version="3.34.0" dbms="SQLITE" exact-version="3.34.0" exact-driver-version="3.34"><identifier-quote-string>&quot;</identifier-quote-string></database-info><case-sensitivity plain-identifiers="mixed" quoted-identifiers="mixed"/><driver-ref>sqlite.xerial</driver-ref><synchronize>true</synchronize><jdbc-driver>org.sqlite.JDBC</jdbc-driver><jdbc-url>jdbc:sqlite:$USER_HOME$/Documents/Моя папка/DB_SQLITE/PowerManagerDatabase/database.sqlite</jdbc-url><secret-storage>master_key</secret-storage><auth-provider>no-auth</auth-provider><schema-mapping><introspection-scope><node kind="schema" qname="main"/></introspection-scope></schema-mapping><working-dir>$ProjectFileDir$</working-dir></data-source>
#END#
--%>
<%
  if (request.getParameter("btn_delete") != null) {

    String query = "delete from main.People where ID=" + request.getParameter("btn_delete");
    DBAdapter2.executeSQL(query);
  }

  if (request.getParameter("btn_edit") != null) {

    String query = "select * from main.People where ID=" + request.getParameter("btn_edit");
    ResultSet rs = DBAdapter2.executeQuery(query);

    request.setAttribute("iID", rs.getInt("ID"));
    request.setAttribute("iName", rs.getString("NAME"));
    request.setAttribute("iAddress", rs.getString("Address"));
    request.setAttribute("iBirthday", rs.getString("Birthday"));
    request.setAttribute("iAction", "Save");

    rs.close();
    DBAdapter2.getStatement().close();
    DBAdapter2.getConnection().close();

  }
  else
  {
    request.setAttribute("iID", "");
    request.setAttribute("iName", "");
    request.setAttribute("iAddress", "");
    request.setAttribute("iBirthday", "");
    request.setAttribute("iAction", "Insert");
  }

  if (request.getParameter("btn_save") != null) {

      String query;
      if (request.getParameter("btn_save") != "") {
        query = "update main.People set Name =\"" + request.getParameter("edit_name") + "\", Address =\"" + request.getParameter("edit_address") + "\", Birthday =\"" + request.getParameter("edit_birthday") + "\" where ID=" + request.getParameter("btn_save");
      }
      else{
        query = "insert into main.People (Name, Address, Birthday) select \"" + request.getParameter("edit_name") + "\", \"" + request.getParameter("edit_address") + "\", \"" + request.getParameter("edit_birthday") + "\"";
      }

    DBAdapter2.executeSQL(query);

  }

%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
  <head>
    <title>MyWebApp</title>
  </head>
  <body>
  Записи базы данных:
  </body>
  <form method="post" >

    <table border="2">
      <tr>
        <td>ID</td>
        <td>NAME</td>
        <td>ADDRESS</td>
        <td>BIRTHDAY</td>
        <td>Edit</td>
        <td>Delete</td>
      </tr>
      <%
        try
        {

          DBAdapter2.setUrlConnection(request.getSession().getServletContext().getRealPath("/")+"task4.db");
          String query = "select * from main.People";
          ResultSet rs = DBAdapter2.executeQuery(query);
          while(rs.next())
          {

      %>
      <tr><td><%=rs.getInt("ID") %></td>
      <td><%=rs.getString("NAME") %></td>
      <td><%=rs.getString("Address") %></td>
      <td><%=rs.getString("Birthday") %></td>

        <td><div> <input type="submit" style="background-image: url(edit.jpg); color: transparent; border:none; background-repeat:no-repeat;background-size:18px 18px;"  name="btn_edit" value=<%=rs.getInt("ID") %> ></div></td>
        <td><div> <input type="submit" style="background-image: url(delete.jpg); color: transparent; border:none; background-repeat:no-repeat;background-size:18px 18px;"  name="btn_delete" value=<%=rs.getInt("ID") %> ></div></td>

      <%

        }
      %>
    </table>
    <table border="2">
      <tr>
        <td>ID</td>
        <td>NAME</td>
        <td>ADDRESS</td>
        <td>BIRTHDAY</td>
        <td>Action</td>
      </tr>
    <tr>
      <td><div><input type="text" name="edit_id" disabled="true" value=<%=request.getAttribute("iID") %>></div></td>
      <td><div><input type="text" name="edit_name"  value=<%=request.getAttribute("iName") %>></div></td>
      <td><div><input type="text" name="edit_address"  value=<%=request.getAttribute("iAddress") %>></div></td>
      <td><div><input type="text" name="edit_birthday"  value=<%=request.getAttribute("iBirthday") %>></div></td>
      <td><div> <input type="submit" style="background-image: url(save.jpg); color: transparent; border:none; background-repeat:no-repeat;background-size:18px 18px;"  name="btn_save" value=<%=request.getAttribute("iID") %> ></div></td>
    </tr>
    </table>
    <%
        rs.close();
        DBAdapter2.getStatement().close();
        DBAdapter2.getConnection().close();
      }
      catch(Exception e)
      {
        e.printStackTrace();
      }




    %>

  </form>
  <form action="upload" method="post" enctype="multipart/form-data">
    <table border="0">
      <tr>
        <td>Добавить записи из файла в базу данных:</td>
        <td></td>
      </tr>
      <tr>
        <td><input type="file" name="file"  /></td>
        <td><input type="submit" value="Загрузить" /><td>
      </tr>
    </table>
  </form>
</html>
