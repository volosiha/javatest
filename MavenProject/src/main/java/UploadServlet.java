import DBPackage.DBAdapter2;

import javax.servlet.*;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.*;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

@WebServlet("/upload")
@MultipartConfig
public class UploadServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        Part filePart = request.getPart("file");
        InputStream fileContent = filePart.getInputStream();

        try {


            String query;

            try (BufferedReader reader = new BufferedReader(new InputStreamReader
                    (fileContent, Charset.forName(StandardCharsets.UTF_8.name())))) {

                String s;

                while ( (s = reader.readLine()) != null) {

                    s = s.substring(s.indexOf(";")+1);
                    s = s.replace(";", "\",\"");

                    query = "insert into main.People (Name, Address, Birthday) select \"" + s + "\"";
                    DBAdapter2.executeSQL(query);
                }
            }

        }
        catch(Exception e)
        {
            e.printStackTrace();
        }

        response.sendRedirect(request.getContextPath() + "/index.jsp");
    }
}
