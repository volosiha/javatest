package DBPackage;

import java.sql.*;

public class DBAdapter2 {
    static  private String urlConnection = "";
    static  private Connection conn = null;
    static  private Statement stmt = null;

    public static String getUrlConnection() {
        return urlConnection;
    }

    public static void setUrlConnection(String urlConnection) {
        DBAdapter2.urlConnection = urlConnection;
    }

    public static Connection getConnection()
    {
        try {
            Class.forName("org.sqlite.JDBC");
            String url = "jdbc:sqlite:" + getUrlConnection();
            String username = "root";
            String password = "root";

            conn = DriverManager.getConnection(url, username, password);


        } catch (Exception e) {
            e.printStackTrace();
        }


        return conn;
    }

    public static Statement getStatement()
    {
        return stmt;
    }

    public static void executeSQL(String sql)
    {
        Connection conn = getConnection();
        if (conn != null) {

            try {
                stmt = conn.createStatement();
                stmt.execute(sql);
                stmt.close();
                conn.close();
            }
            catch (SQLException se)
            {
                se.printStackTrace();
            }
        }

    }

    public static ResultSet executeQuery(String sql)
    {
        ResultSet rs = null;

        Connection conn = getConnection();
        if (conn != null) {

            try {
                stmt = conn.createStatement();
                rs = stmt.executeQuery(sql);

            }
            catch (SQLException se)
            {
                se.printStackTrace();
            }
        }

        return rs;
    }
}
