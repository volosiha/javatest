import org.junit.jupiter.api.Test;
import  org.junit.jupiter.api.Assertions;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

class MainTest {

    @Test
    void main() throws  Exception{

        int n = 8;

        String[] args = new String[1];
        args[0] = String.valueOf(n);
        Main.main(args);

    }
}