import java.io.*;

public class Main {

    public static void main(String[] args)  {

        String fileName = "out.txt";

        int n  = Integer.parseInt(args[0]);

        if ( n <= 0 || n % 2 > 0) {
            System.out.println("Input argument is incorrect. Must be an even integer value grater then 0.");
        }
        else {

           Incrementer incrementer = new Incrementer(fileName);

            IncThread it1 = new IncThread("Thread1", n/2, incrementer);
            IncThread it2 = new IncThread("Thread2", n/2, incrementer);
        }

    }
}
