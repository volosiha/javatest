//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// класс создающий файл, реализующий методы чтения/записи и синхронизированного изменения содержимого файла///
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
import java.io.*;

public class Incrementer {

    private String sFileName;
    private long nThreadID = 0;

    public Incrementer(String FileName){

        sFileName = FileName;

        try {
            BufferedWriter bw = new BufferedWriter(new FileWriter(sFileName));
            bw.write(String.valueOf(0));
            bw.close();
        }
        catch (
                IOException e)
        {
            e.printStackTrace();
        }
    }

    private int Read() throws IOException{
        String s;
        int v = 0;
        BufferedReader br = null;

        try {

            br = new BufferedReader(new FileReader(sFileName));
            if ((s = br.readLine()) != null) {
                v = Integer.parseInt(s);
            }
        }
        catch (
                IOException e)
        {
            e.printStackTrace();
        }
        finally {
            if (br != null) {
                br.close();
            }
        }

        return v;
    }

    private void Write(int v) throws IOException {
        BufferedWriter bw = null;

        try {

            bw = new BufferedWriter(new FileWriter(sFileName));
            bw.write(String.valueOf(v));
        }
        catch (
                IOException e)
        {
            e.printStackTrace();
        }
        finally {
            if (bw != null) {
                bw.close();
            }
        }
    }

    private void OutPrint(int v)
    {
        int v1 = v+1;
        System.out.println(v + ", " + v1 + ", Current thread id is: " + Thread.currentThread().getId());
    }

    public synchronized void DoInc(){

        if (nThreadID == Thread.currentThread().getId()) {

            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        int v = 0;

        try {
            v= Read();
            OutPrint(v);
            Write(v + 1);
        }
        catch (IOException e) {
            e.printStackTrace();
        }

        nThreadID = Thread.currentThread().getId();
        notify();

    }
}
