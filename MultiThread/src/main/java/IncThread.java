//////////////////////////////////////////////////////////////////////////////////////////
// класс потока, который в цикле инкриментирует значение в файле указанное количество раз//
//////////////////////////////////////////////////////////////////////////////////////////
public class IncThread  implements Runnable {
    private int nCount;
    private Incrementer iIncrementer;

    public IncThread(String ThreadName, int n, Incrementer incrementer)
    {
        nCount = n;
        iIncrementer = incrementer;
        new Thread(this, ThreadName).start();
    }

    @Override
    public void run() {

        for (int i = 0; i < nCount; i++) {
            iIncrementer.DoInc();
        }
    }
}
