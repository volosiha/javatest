///////////////////////////////////////////////////////////////////////////////////////////////
// класс содержащий статические методы рекурсивного поиска по заданной матрице коэффициентов //
///////////////////////////////////////////////////////////////////////////////////////////////
public abstract class MatrixSeeker {
    private static Float [][]kMatrix;

    public static Float Search(Float [][]km, int iv1,int iv2){
        kMatrix = km;

        return Search(iv1,iv2,iv1);
    }

    private static Float Search(int iv1, int iv2, int iex){
        Float f=null;

        if (iv1 == iv2) {
            f = (float) 1;
        }
        else {
            if (kMatrix[iv1][iv2] != null){
                f = kMatrix[iv1][iv2];
            }
            else{
                for (int k = 0; k < kMatrix[iv1].length; k++)
                {
                    if (kMatrix[iv1][k] != null && k != iex && k != iv1) {
                        Float f1 =   kMatrix[iv1][k];
                        Float f2 = Search(k, iv2,iv1);
                        if (f2 != null){
                            f = f1*f2;
                        }
                        break;
                    }

                }
            }
        }

        return f;

    }
}
