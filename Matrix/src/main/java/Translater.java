///////////////////////////////////////////////////////////////////////////////////////
// класс, выполняющий конверсию величин                                             //
///////////////////////////////////////////////////////////////////////////////////////
import java.io.*;

public class Translater {
    private final String sIn;
    private final String sOut;
    private final RelationList Relations;
    private final RelationList Questions;
    private final ValueList Values;

    public Translater(String in, String out) {

        sIn = in;
        sOut = out;

        Relations = new RelationList();
        Questions = new RelationList();
        Values = new ValueList();
    }

    public void Run(){

        try {
            ReadRelations();
            FindAnswers();
            WriteAnswers();
        } catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    //прочитаем условия и вопросы
    private void ReadRelations() throws IOException{

        BufferedReader br = null;
        String s;

        boolean bQ = false;

        try
        {
            br = new BufferedReader(new FileReader(sIn));

            while ((s = br.readLine()) != null) {
                if (!s.equals("")) {
                    if (!bQ) {
                        Relations.add(new Relation(s));
                    } else {
                        Questions.add(new Relation(s));
                    }
                } else {
                    bQ = true;
                }
            }

            for (Relation r :  Relations) {

                if (!Values.contains(r.getV1()))
                    Values.add(r.getV1());

                if (!Values.contains(r.getV2()))
                    Values.add(r.getV2());
            }
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        finally {
            if (br != null) {
                br.close();
            }
        }
    }

    //составим матрицу коэфициентов и найдем ответы на вопросы
    private void FindAnswers(){

        //создадим матрицу коэффициентов
        Float[][] kMatrix = new Float[Values.size()][Values.size()];

        for (int k = 0; k < Values.size(); k++) {
            kMatrix[k][k] = (float) 1;
        }

        for (Relation rk : Relations) {

            kMatrix[Values.indexOf(rk.getV2())][Values.indexOf(rk.getV1())] = rk.getK1() / rk.getK2();
            kMatrix[Values.indexOf(rk.getV1())][Values.indexOf(rk.getV2())] = rk.getK2() / rk.getK1();
        }


        for (Relation q : Questions) {

            Float ff = MatrixSeeker.Search(kMatrix, Values.indexOf(q.getV1()), Values.indexOf(q.getV2()));
            if (ff != null) {
                q.SetK2(ff*q.getK1());
            }
        }
    }

    //запишим ответы в выходной файл
    private void WriteAnswers() throws IOException {
        String s;
        BufferedWriter bw = null;

        try {

            bw = new BufferedWriter(new FileWriter(sOut));

            for (Relation q : Questions) {
                if (q.getK2() != 0) {
                    s = q.ToString() + "\n";
                } else {
                    s = "конверсия не возможна\n";
                }

                bw.write(s);

            }
        }
        catch (
                IOException e)
        {
            e.printStackTrace();
        }
        finally {
            if (bw != null) {
                bw.close();
            }
        }
    }
}
