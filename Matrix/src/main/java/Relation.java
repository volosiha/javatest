//////////////////////////////////////////////////////////////////////////////
//   класс описывающий выражение одной величины через другую                //
//////////////////////////////////////////////////////////////////////////////
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Relation {
    private Float k1 = (float) 0;
    private Float k2 = (float) 0;
    private String v1 = "";
    private String v2 = "";

    public Relation(String s) {
        s = s.trim();
        s = " " + s + " ";
        s = s.replace(" = "," ");
        s = s.replace("?", "0");

        Pattern pK = Pattern.compile("\\s.+?\\s");
        Matcher mK = pK.matcher(s);

        while (mK.find()) {

            if (k1 == 0) {

                k1 = Float.parseFloat(s.substring(mK.start()+1, mK.end()));
            }
            else {
                k2 = Float.parseFloat(s.substring(mK.start()+1, mK.end()));
            }
        }

        Pattern pV = Pattern.compile("\\d\\s.+?\\s");
        Matcher mV = pV.matcher(s);

        while (mV.find()) {

            if (v1.equals("")){
                v1 = s.substring(mV.start()+2, mV.end());
            }
            else
            {
                v2 = s.substring(mV.start()+2, mV.end());
            }
        }
    }

    public Float getK1()
    {
        return k1;
    }

    public String getV1()
    {
        return v1;
    }

    public Float getK2()
    {
        return k2;
    }

    public String getV2()
    {
        return v2;
    }

    public void SetK2(float f)
    {
        k2 = f;
    }

    public void PrintOut()
    {
        System.out.println(ToString());
    }

    public String ToString()
    {
        String s;

        s = getK1().toString() + " " + getV1() + " = " + getK2().toString() + " " + getV2();
        return s;
    }
}
