import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MatrixSeekerTest {

    @Test
    void search() {

        Float [][]Matrix = {{(float)1.0,(float)2.0,null},{(float)0.5,(float)1,(float)10}, {null,(float)0.1,(float)1}};
        Float f =  MatrixSeeker.Search(Matrix, 0, 2);
        assertEquals(20,f);

    }
}